﻿using System.Linq;
using System.Web.Mvc;
using ShakespeareWeb.Services;

namespace ShakespeareWeb.Controllers
{
    public class SearchController : Controller
    {
        private readonly ISearchService _searchService;

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
        }

        [HttpGet]
        public ActionResult Index(string keywords)
        {
            if (string.IsNullOrEmpty(keywords))
                return View("NoResults");

            var model = _searchService.SearchByKeyword(Server.HtmlEncode(keywords));

            return (model.Any())
                       ? View(model)
                       : View("NoResults");
        }

    }
}
