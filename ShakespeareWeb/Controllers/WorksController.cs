﻿using System.Web.Mvc;
using ShakespeareWeb.Services;

namespace ShakespeareWeb.Controllers
{
    public class WorksController : Controller
    {
        private readonly IWorksService _service;

        public WorksController(IWorksService service)
        {
            _service = service;
        }

        public ActionResult Index()
        {
            return View(_service.GetAllWorks());
        }

        public ActionResult Detail(int id)
        {
            return View(_service.GetById(id));
        }

        [HttpGet]
        public ActionResult Search()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string keywords)
        {
            return View((keywords));
        }
    }
}
