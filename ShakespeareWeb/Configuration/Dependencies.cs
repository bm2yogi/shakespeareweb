using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;

namespace ShakespeareWeb.Configuration
{
    public static class Dependencies
    {
        public static void RegisterDependencies()
        {
            var applicationAssembly = Assembly.GetExecutingAssembly();
            var builder = new ContainerBuilder();

            builder.RegisterControllers(applicationAssembly);
            builder.RegisterModelBinders(applicationAssembly);
            builder.RegisterAssemblyTypes(applicationAssembly).AsImplementedInterfaces();
            builder.RegisterModelBinderProvider();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}