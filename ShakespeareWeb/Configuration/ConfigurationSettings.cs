using System;
using System.Configuration;

namespace ShakespeareWeb.Configuration
{
    public interface IConfigurationSettings
    {
        string ServiceUri { get; }
    }

    public class ConfigurationSettings : IConfigurationSettings
    {
        private const string ServiceUriKey = "serviceUri";
        
        public string ServiceUri
        {
            get { return (ConfigurationManager.AppSettings[ServiceUriKey]); }
        }
    }
}