using System;
using System.IO;
using System.Net;
using System.Text;

namespace ShakespeareWeb.Configuration
{
    public interface IWebClient
    {
        string GetJsonData(string uri);
        string Post(string uri, string keywords);
    }

    public class ShakespeareWebClient : IWebClient
    {
        public string GetJsonData(string uri)
        {
            return new WebClient().DownloadString(uri);
        }

        public string Post(string requestUri, string jsonRequest)
        {
            var jsonResponse = "";
            var request = BuildWebRequest(jsonRequest, new Uri(requestUri));
            var response = (HttpWebResponse)request.GetResponse();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                jsonResponse = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }

            return jsonResponse;
        }

        private HttpWebRequest BuildWebRequest(string jsonRequest, Uri requestUri)
        {
            var request = (HttpWebRequest)WebRequest.Create(requestUri);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/json";
            request.ContentLength = Encoding.UTF8.GetByteCount(jsonRequest);

            var writer = new StreamWriter(request.GetRequestStream());
            writer.Write(jsonRequest);
            writer.Flush();

            return request;
        }


    }
}