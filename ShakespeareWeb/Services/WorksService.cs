using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ShakespeareWeb.Configuration;
using ShakespeareWeb.Models;

namespace ShakespeareWeb.Services
{
    public interface IWorksService
    {
        IEnumerable<Work> GetAllWorks();
        Work GetById(int id);
    }

    public class WorksService : IWorksService
    {
        private readonly IWebClient _webClient;
        private readonly string _host;
        private const string Path = "/works";

        public WorksService(IConfigurationSettings configuration, IWebClient webClient)
        {
            _webClient = webClient;
            _host = configuration.ServiceUri;
        }

        public IEnumerable<Work> GetAllWorks()
        {
            var uri = new UriBuilder
                          {
                              Host = _host,
                              Path = Path
                          }.ToString();

            var jsonData = _webClient.GetJsonData(uri);

            return JsonConvert.DeserializeObject<IEnumerable<Work>>(jsonData);
        }

        public Work GetById(int id)
        {
            var path = string.Format("{0}/{1}", Path, id);
            var uri = new UriBuilder {Host = _host, Path = path}.ToString();
            var jsonData = _webClient.GetJsonData(uri);
            return JsonConvert.DeserializeObject<Work>(jsonData);
        }
    }
}