using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ShakespeareWeb.Configuration;
using ShakespeareWeb.Models;

namespace ShakespeareWeb.Services
{
    public interface ISearchService
    {
        IEnumerable<Work>SearchByKeyword(string keywords);
    }

    public class SearchService : ISearchService
    {
        private readonly IWebClient _webClient;
        private readonly string _host;
        private const string Path = "/search";

        public SearchService(IConfigurationSettings configuration, IWebClient webClient)
        {
            _webClient = webClient;
            _host = configuration.ServiceUri;
        }

        public IEnumerable<Work>SearchByKeyword(string keywords)
        {
            var requestUri = new UriBuilder
                              {
                                  Host = _host,
                                  Path = Path,
                                  Query = string.Format("keywords={0}", (keywords))
                              }.ToString();

            var jsonData = _webClient.GetJsonData(requestUri);

            return JsonConvert.DeserializeObject<IEnumerable<Work>>(jsonData);
        }
    }
}