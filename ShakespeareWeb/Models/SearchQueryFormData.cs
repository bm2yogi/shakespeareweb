﻿namespace ShakespeareWeb.Models
{
    public class SearchQueryFormData
    {
        public string Keywords { get; set; }
    }
}