namespace ShakespeareWeb.Models
{
    public class Work
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}