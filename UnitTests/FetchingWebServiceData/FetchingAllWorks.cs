﻿using System;
using Moq;
using NUnit.Framework;
using Sahara;
using ShakespeareWeb.Configuration;
using ShakespeareWeb.Controllers;
using ShakespeareWeb.Services;

namespace UnitTests.FetchingWebServiceData
{
    [TestFixture]
    public class FetchingAllWorks
    {
        private IConfigurationSettings configuration;
        private IWebClient webClient;

        [TestFixtureSetUp]
        public void SetupContext()
        {
            //var mockConfig = new Mock<IConfigurationSettings>();
            //var mockClient = new Mock<IWebClient>();

            configuration = new ConfigurationSettings();
            webClient = new ShakespeareWebClient();
        }

        [Test]
        public void It_should_get_the_data()
        {
            new WorksService(configuration, webClient).GetAllWorks().ShouldNotBeNull();
        }
    }
}